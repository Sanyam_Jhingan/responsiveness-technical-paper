# Responsive Web Design

Responsive design is an approach to web design that makes your web content adapt to the different screen and window sizes of a variety of devices.

## Why Responsive Web Design Matters

The answer is simple. It’s no longer enough to design for a single device. Mobile web traffic has overtaken desktop and now makes up the majority of website traffic, accounting for more than 51%.

![Desktop vs Mobile market share](https://kinsta.com/wp-content/uploads/2020/09/web-traffic.png)


## Responsive Web Design vs Adaptive Design

The difference between responsive design and adaptive design is that responsive design adapts the rendering of a single page version. In contrast, adaptive design delivers multiple completely different versions of the same page.

![Adaptive vs Responsive Web Design](https://kinsta.com/wp-content/uploads/2020/08/responsive-adaptive-design.png)

With responsive design, users will access the same basic file through their browser, regardless of device, but CSS code will control the layout and render it differently based on screen size. With adaptive design, we use media queries that check for the screen size, and then accesses the template designed for that device.

## The Building Blocks of Responsive Web Design

In this section, we will cover the building blocks of responsive Web Design:

### 1. Media Queries

A media query is a fundamental part of CSS3 that lets you render content to adapt to different factors like screen size or resolution.

![Media query](https://kinsta.com/wp-content/uploads/2020/08/media-queries.png)

It works in a similar way to an ***if clause*** in some programming languages, basically checking if a screen’s viewport is wide enough or too wide before executing the appropriate code.

```css
@media (max-width: 600px) {
  body {
    background-color: lightblue;
  }
}
```

### 2. Fluid Layout

A fluid layout relies instead on dynamic values like a percentage of the viewport width.

![fluid-layout](https://kinsta.com/wp-content/uploads/2020/08/fluid-layout.png)

This approach will dynamically increase or decrease the different container element sizes based on the size of the screen.

### 3. Flexbox Layout

Flexbox is a CSS module designed as a more efficient way to lay out multiple elements, even when the size of the contents inside the container is unknown.

![flexbox](https://kinsta.com/wp-content/uploads/2020/08/flexbox-justify.png)

A flex container expands items to fill available free space or shrinks them to prevent overflow. These flex containers have a number of unique properties, like justify-content, that you can’t edit with a regular HTML element.

### 4. Speed

Your approach to responsiveness must not block or delay your page’s first render any more than it needs to. We can make our pages load faster by:

- Optimizing our images
- Implementing caching
- Using a more efficient CSS layout
- Avoiding render-blocking JS

### 5. Responsive Images

To serve different versions scaled for different devices, we use HTML `srcset` attribute in our `img` tags, to specify more than one image size to choose from.

```HTML
<img src="./tree-planting.png" 
    srcset="./tree-planting.png 576w,
    ./tree-planting@2x.png 1152w,
    ./tree-planting@3x.png 1728w" 
    sizes="(max-width: 600px) 576px,
           (max-width: 1200px) 1152px,
           1728px">
```

Using this approach every user doesn't have to download the full-sized image, even on mobile.

### 6. Responsive Typography

You should also adjust your font-sizes appropriately to match screen size. The easiest way to do so is to set a static value for font-size, like 22 px, and adapt it in each media query.

![responsive-typography](https://kinsta.com/wp-content/uploads/2020/08/2-scatter-plot-font-size-opt.png)

You can target multiple text elements at the same time by using a comma to separate each one.

```CSS
@media (min-width: 1200px) {
    body {
        font-size: 16px;
    }
}
```

## Mobile First approach

If you choose a mobile-first approach to design, with a single column and smaller font sizes as the basis, you don’t need to include mobile breakpoints, unless you want to optimize the design for specific models.

![mobile-first](https://kinsta.com/wp-content/uploads/2020/08/mobile-first.png)

So you can create a basic responsive design with just two breakpoints, one for tablets and one for laptops and desktop computers.

## CSS Units and Values for Responsive Design

CSS has both absolute and relative units of measurement. An example of an absolute unit of length is a cm or a px. Relative units or dynamic values depend on the size and resolution of the screen or the font sizes of the root element.

### PX vs EM vs REM vs Viewport Units for responsive design

- PX : a single pixel
- EM : relative unit based on the font-size of the element
- REM : relative unit based on the font-size of the element
- VH, VW : % of the viewport's height or width
- % : the percentage of the parent element

Setting the width and max-width of images and other elements, using % is the best solution. This approach will make sure the components adjust to the screen size of every device.

